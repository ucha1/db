const mix = require('laravel-mix');

mix.disableNotifications()
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/games/demo/boot.js', 'public/js/games/demo.js')
    .vue({version: 2,})
    .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig({
        watchOptions: {ignored: /node_modules/},
        module      : {
            rules: [
                {
                    test   : /\.ts?$/,
                    loader : "ts-loader",
                    options: {appendTsSuffixTo: [/\.vue$/]},
                    exclude: /node_modules/
                }
            ]
        },
        resolve     : {
            extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"]
        }
    });
