<?php

return [

    /*
    * This class can modify the output returned by Tinker. You can replace this with
    * any class that implements \App\Http\OutputModifiers\OutputModifier.
    */
    'output_modifier' => \App\Http\OutputModifiers\PrefixDateTime::class,

    /*
     * If you want to fine-tune PsySH configuration specify
     * configuration file name, relative to the root of your
     * application directory.
     */
    'config_file' => env('PSYSH_CONFIG', null),
];
