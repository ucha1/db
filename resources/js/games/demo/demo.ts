import {BootScene} from "./scenes/bootScene";
import {MenuScene} from "./scenes/mainMenuScene";
import {GameScene} from "./scenes/gameScene";
import {HUDScene} from "./scenes/HUDScene";

// main game
const tute = new Phaser.Game({
    title: "Tate-Tute",
    version: "1.0",
    width: 170,
    height: 150,
    zoom: 5,
    type: Phaser.CANVAS,
    parent: "demo",
    backgroundColor: "#f8f8f8",
    scene: [BootScene, MenuScene, HUDScene, GameScene],
    input: {
        keyboard: true,
        mouse: false,
        touch: false,
        gamepad: false
    },
    physics: {
        default: "arcade",
        arcade: {
            gravity: {y: 475},
            debug: false
        }
    },
    render: {pixelArt: true, antialias: true}
});
