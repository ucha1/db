require('./bootstrap');
import Vue from "vue";
import Vuetify from "vuetify";
import VueSplit from 'vue-split-panel'
import VueCodemirror from 'vue-codemirror'
import VueJsonPretty from 'vue-json-pretty'
import Echo from 'laravel-echo';

const client = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    //client: client,
    key: process.env.MIX_PUSHER_APP_KEY,
    wsHost: window.location.hostname,
    wsPort: 6001,
    wssPort: 6001,
    forceTLS: true,
    disableStats: true,
    enabledTransports: ['ws','wss'],
    // authorizer: (channel, options) => {
    //     return {
    //         authorize: (socketId, callback) => {
    //             axios.post('/api/broadcasting/auth', {
    //                 socket_id: socketId,
    //                 channel_name: channel.name
    //             }).then(({data}) => {
    //                 callback(false, data);
    //
    //             }).catch(error => {
    //                 callback(true, error);
    //             });
    //         }
    //     };
    // }
});


Vue.use(Vuetify);
// you can set default global options and events when use
Vue.use(VueCodemirror, {
    options: {
        theme                    : 'default',
        tabSize                  : 4,
        cursorBlinkRate          : 300,
        autoRefresh              : {delay: 300},
        autofocus                : true,
        line                     : true,
        lineNumbers              : true,
        lineWrapping             : false,
        styleActiveLine          : true,
        styleSelectedText        : true,
        autoCloseBrackets        : true,
        foldGutter               : true,
        fixedGutter              : true,
        gutters                  : ["CodeMirror-foldgutter", "CodeMirror-linenumbers"],
        highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true},
        hintOptions              : {
            completeSingle: true,
        },
        lineComment              : "//",
        blockCommentStart        : '/*',
        blockCommentEnd          : '*/',
        commentBlankLines        : true,
        continueLineComment      : true,
        keyMap                   : "sublime",
        matchBrackets            : true,
        showCursorWhenSelecting  : true,
    },
});

// pretty json
Vue.component("vue-json-pretty", VueJsonPretty);

//split view
Vue.use(VueSplit);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    vuetify: new Vuetify,
    el     : '#app',
});


// Clippy For Fun
import clippy from 'clippyjs'
window.clippy = clippy


clippy.load('Genius', function(agent){
    window.clippyAgent = agent;
    clippyAgent.moveTo(window.innerWidth-150,window.innerHeight-150);
    String(process.env.MIX_CLIPPY_ENABLE) == "true" ? clippyAgent.show() : clippyAgent.hide();
    // clippyAgent.animate();
}, undefined, '/js/clippyjs/assets/agents/');

console.log(String(process.env.MIX_CLIPPY_ENABLE));


