<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{env("APP_NAME")}}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Icon -->
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon"/>
    @yield('css')
</head>
<body>
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<div id="app">
    @yield('content')
</div>

<div id="scripts">
    @yield('scripts')
</div>
</body>
</html>
