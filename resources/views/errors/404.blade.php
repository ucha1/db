@extends('layouts.app')

@section('content')
    <layout app>
        <div>
            <v-app-bar app>
                <v-spacer></v-spacer>

            </v-app-bar>
            <v-content style="background-color: #e4cfa4" app>

                <div class="text-lg-center " >
                    <h2 class="glow">You should not be here !</h2><br>
                    <h3 class="glow">Go back where you come from my son !</h3>

                    <a href="/">
                        <img width="700" src="{{asset('/img/threeGentelmen.png')}}" alt="">
                    </a>
                </div>
            </v-content>
        </div>

    </layout>
@endsection
@section('css')
    <style>
        .glow {
            color: #fff;
            text-align: center;
            -webkit-animation: glow 1s ease-in-out infinite alternate;
            -moz-animation: glow 1s ease-in-out infinite alternate;
            animation: glow 1s ease-in-out infinite alternate;
        }

        @-webkit-keyframes glow {
            from {
                text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #e6e53f, 0 0 40px #e4e627, 0 0 50px #e60073, 0 0 60px #e60073, 0 0 70px #e60073;
            }
            to {
                text-shadow: 0 0 20px #fffadd, 0 0 30px #fff7c9, 0 0 40px #fffa58, 0 0 50px #fffb36, 0 0 60px #ff4da6, 0 0 70px #ff4da6, 0 0 80px #ff4da6;
            }
        }
    </style>
@endsection
