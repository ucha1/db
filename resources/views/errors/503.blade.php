@extends('layouts.app')

@section('content')
    <v-content app>

        <v-footer padless>
            <v-col class="text-center" cols="12" >
                <h3>503 | Down for Maintenance!</h3>
            </v-col>
        </v-footer>

    </v-content>
@endsection
