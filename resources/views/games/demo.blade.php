@extends('layouts.app2')

@section('content')

    <layout app>
        <v-container class="grey lighten-5" app>
            <v-row no-gutters justify="center" >
                <div id="demo"></div>
            </v-row>
        </v-container>
    </layout>

@endsection
@section('scripts')
    <script src="{{ asset('js/games/demo.js') }}"></script>
@endsection
