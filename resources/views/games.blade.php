@extends('layouts.app2')

@section('content')
    <layout app>
        <v-container class="grey lighten-5" app>
            <v-row no-gutters justify="center">
                <v-col>
                    <v-card class="mx-auto" max-width="344" outlined href="/games/demo">
                        <v-list-item three-line>
                            <v-list-item-content>
                                <v-list-item-title class="headline mb-1">Demo</v-list-item-title>
                                <v-list-item-subtitle>This is an example Project</v-list-item-subtitle>
                            </v-list-item-content>
                            <v-list-item-avatar tile size="80" color="grey"></v-list-item-avatar>
                        </v-list-item>
                    </v-card>
                </v-col>
            </v-row>
        </v-container>
    </layout>


@endsection
