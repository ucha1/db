@extends('layouts.app')

@section('scripts')
    <script src="{{asset('js/sqlToBuilder/sql-parser.js')}}"></script>
    <script src="{{asset('js/sqlToBuilder/builder.js')}}"></script>
@endsection

@section('content')
    <layout app>

        <sql-layout></sql-layout>

    </layout>
@endsection
