## About this project
This is a developer tool, to easily navigate/query thought out
database and explore all available api endpoints. Also you can execute quick php scripts in a browser which is specific to our infrastructure.

With this tool you could quickly query database or 
just perform All Laravel/PHP operations and test your
code in browser or use scrotis api library Scrolavel
or explore endpoints and send requests.

## Installation 

`composer install` and for frontend `npm install`

copy `.env.example` file to `.env` and adjust accordingly

## Screenshots
===
---
![](screen_1.png)
![](screen_2.png)
![](screen_3.png)

