<?php

use App\Http\Controllers\GameController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RequestHandlerController;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;

//SQL Sandbox
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/qlf', [HomeController::class, 'qlf']);
Route::post('/sql', [HomeController::class, 'sql']);

//PHP Sandbox
Route::get('/php', [HomeController::class, 'php']);
Route::get('/version', 'HomeController@version');
Route::post('/execute', [HomeController::class, 'execute']);

//home routes
Route::get('/explore', [HomeController::class, 'explore']);
Route::get('/terminal', [HomeController::class, 'terminal']);
Route::get('/getExplorerData', [HomeController::class, 'getExplorerData']);
Route::post('/getDocsBy', [HomeController::class, 'getDocsBy']);
Route::get('/history/{type}/{ip?}', [HomeController::class, 'getHistory']);

Route::get('/visitors', [RequestHandlerController::class, 'visitors']);
Route::post('/request/get', [RequestHandlerController::class, 'getHandler']);
Route::post('/request/post', [RequestHandlerController::class, 'anyHandler']);
Route::put('/request/put', [RequestHandlerController::class, 'anyHandler']);
Route::delete('/request/delete', [RequestHandlerController::class, 'anyHandler']);



//Game routes
Route::get('/games', [GameController::class, 'index']);
Route::get('/games/demo', [GameController::class, 'demo']);



Route::get('/test', function () {
    return ["TEST"];
});

//Site Maintenance
Route::get('/ping', [RequestHandlerController::class, 'ping']);
Route::get('/down', function () { return \Artisan::call('down --allow=127.0.0.1'); });
Route::get('/up', function () { return \Artisan::call('up'); });

//Route::get('/broadcast', function () {
//    //dd(session('visitor.ip'));
//    \broadcast(new \App\Events\Visitor());
//    return collect(['Broadcasted']);
//});
