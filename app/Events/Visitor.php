<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Visitor implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $visitor;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $visitor)
    {
        $this->visitor = $visitor;
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    // public function broadcastAs()
    // {
    //     return 'visitor';
    // }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
//    public function broadcastWith()
//    {
//        return ['visitor' => cache()->get('visitor')];
//    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('channel-visitor');
//        return new PrivateChannel('channel-visitor');
//        return new PresenceChannel('channel-visitor');
    }
}
