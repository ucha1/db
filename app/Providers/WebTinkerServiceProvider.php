<?php

namespace App\Providers;

use App\Http\OutputModifiers\OutputModifier;
use Illuminate\Support\ServiceProvider;

class WebTinkerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../../config/web-tinker.php' => config_path('web-tinker.php'),
            ], 'config');
        }

        $this->app->bind(OutputModifier::class, config('web-tinker.output_modifier'));
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/web-tinker.php', 'web-tinker');
    }

}
