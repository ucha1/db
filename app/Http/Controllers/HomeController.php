<?php

namespace App\Http\Controllers;

use App\Http\Tinker;
use C4S\API;
use C4S\Scrotis;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use function foo\func;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        API::setHost(env("MIX_SCROTIS_URL"));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function qlf()
    {
        return view('qlf');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function php()
    {
        return view('php', ["version" => phpversion()]);
    }

    /**
     * Explore Scrotis API
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function explore()
    {
        return view('explore');
    }

    /**
     * Connect to server from db
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function terminal()
    {
        return view('terminal');
    }

    /**
     * Return data fro frontend
     */
    public function getExplorerData()
    {
        return collect(["get", "post", "put", "delete"])
            ->flatMap(function ($type) {
                return $this->filterRoutes($type);
            });
    }

    /**
     * Return router params or payload by endpoint
     *
     * @param  Request  $request
     * @return Collection|\Tightenco\Collect\Support\Collection
     * @throws \ReflectionException
     */
    public function getDocsBy(Request $request)
    {
        if (!$request->get("endpoint")) {
            return null;
        }
        $endpoint   = Str::of($request->get("endpoint")["name"])->explode('/')->filter();
        $controller = $endpoint->first();
        $method     = Str::camel($endpoint->slice(2,1)->first(null,'show'));

        if(Str::contains($controller,['{','}'])){
            return null;
        }


        try {
            $class = ("\C4S\\".ucfirst($controller));
            include_once base_path()."/vendor/c4s/scrolavel/src/Scrotis.php";
            $rc = new \ReflectionClass(new $class());
        } catch (\Exception| \Error | \ReflectionException $e) {
            return collect("Class $class NOT Found");
        }


        $method = Str::of($rc->getDocComment())->after("@method Scrotis")->match("/$method/i");
        $str    = Str::of($rc->getDocComment())
            ->after("@method Scrotis $method")
            ->before("*")
            ->replaceMatches("/\(.*\)/", '');

        if (!$str->match("/\@param/")->isEmpty()) {
            $param = $str->between("@param [", "]")->explode(',')->map(function ($item) {
                return trim($item);
            });
        }

        if (!$str->match("/\@payload/")->isEmpty()) {
            $payload = $str->between("@payload [", "]")->explode(',')->flatMap(function ($item) {
                return [trim($item) => ''];
            });
        }


        $doc = collect(API::get("/routes/nid/getDocBy?endpoint=".$request->get("endpoint")["name"]))->first();

        return collect(["param" => $param ?? '', "payload" => $payload ?? '', 'doc' => $doc]);
    }

    /** helper function
     *
     * @param  string  $type
     * @return Collection
     */
    private function filterRoutes(string $type): Collection
    {
        $routes = API::get("/routes/nid/getAllRoutes");
        return collect($routes->{strtolower($type)})
            ->filter(function ($item) {
                return Str::of($item)->startsWith("/");
            })
            ->map(function ($item) {
                return "".Str::of($item)
                        ->before(' ')
                        ->before('?')
                        ->before('[')
                        ->before(' {')
                        //->replaceMatches('/\modeladmin(.)/', '')
                        ->replaceMatches('/\/wl(.*)/', function ($match) {
                            return '/WL'.ucfirst(str_replace('/wl', '', $match[0]));
                        });
            })->map(function ($item) use ($type) {
                return [
                    "name"  => $item,
                    "group" => strtoupper($type)
                ];
            });
    }

    /** Execute php code. PsyShell
     *
     * @param  Request  $request
     * @param  Tinker  $tinker
     * @return string
     */
    public function execute(Request $request, Tinker $tinker)
    {
        $validated = $request->validate(['code' => 'required']);
        $code      = trim($tinker->cleanOutput($tinker->removeComments($validated['code'])), " \s\t\n\r\0\x0B");
        $this->storeHistory($request->getClientIp()." - ".$code, "/history/php_history.json", 1000);
        return $tinker->execute($validated['code']);
    }

    public function sql(Request $request)
    {
        $query    = $request->get("query");
        $database = $request->get("database");
        try {
            config(['database.connections.mysql.database' => $database]);
            $query = $this->escapeString($query);
            $this->storeHistory($request->getClientIp()." - ".$query, "/history/sql_history.json", 1000);
            return DB::select(DB::raw($query));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $type
     * @param  bool  $dontShowIp
     * @return Collection
     */
    public function getHistory($type, $dontShowIp = true)
    {
        $file = "/history/{$type}_history.json";
        if (!Storage::exists($file)) {
            return collect();
        }

        $take = $dontShowIp ? 10 : 1000;
        return collect(json_decode(Storage::get($file)))->take($take)->when($dontShowIp, function (Collection $collection) {
            return $collection->take(1000)->map(function ($item) {
                return Str::replaceFirst(\request()->getClientIp().' - ', '', $item);
            });
        });
    }

    /**
     * @param  string  $code
     * @param  string  $file
     * @param  int  $size
     * @return void
     */
    private function storeHistory(string $code, string $file = "/history/php_history.json", int $size = 10): void
    {
        if (!Storage::exists($file)) {
            Storage::put($file, '');
        }

        collect([now()->format("Y-m-d H:i:s") => $code])
            ->merge(json_decode(Storage::get($file)))
            ->take($size)
            ->tap(function ($item) use ($file) {
                Storage::put($file, $item);
            });
    }

    private function escapeString(string $data): string
    {
        $non_displayables = [
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        ];
        foreach ($non_displayables as $regex) {
            $data = preg_replace($regex, '', $data);
        }
        $data = str_replace("'", "\"", $data);
        $data = str_replace("`", "", $data);
        return $data;
    }

}
