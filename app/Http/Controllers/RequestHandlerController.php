<?php

namespace App\Http\Controllers;

use C4S\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RequestHandlerController extends Controller
{
    public function __construct()
    {
        API::setHost(env("MIX_SCROTIS_URL", "DEV"));
    }

    public function ping()
    {
        return collect('pong');
    }

    public function visitors()
    {
        return collect([
            'ip'    => session()->get('visitor.ip'),
            'count' => $this->countSessionFiles(),
        ]);
    }

    public function countSessionFiles(): int
    {
        return collect(Storage::disk('spath')->files('/'))->filter(function ($item) {
            return $item !== '.gitignore';
        })->flatten()->count();
    }

    public function getHandler(Request $request)
    {
        $endpoint = $request->get("endpoint");

        return collect(API::get($endpoint));
    }

    public function anyHandler(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'endpoint' => 'required|max:255',
            'payload'  => 'json|max:255',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }

        $payload = collect($request->get("payload"))->toArray();
        return collect(API::post($request->get("endpoint"), $payload));
    }


}
