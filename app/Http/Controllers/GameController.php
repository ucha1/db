<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class GameController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('games');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function demo()
    {
        return view('games/demo');
    }
}
