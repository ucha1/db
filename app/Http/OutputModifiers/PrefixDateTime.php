<?php


namespace App\Http\OutputModifiers;


class PrefixDateTime implements OutputModifier
{
    public function modify(string $output = ''): string
    {
        return '|'.$output;
    }
}
