<?php


namespace App\Http\OutputModifiers;


interface OutputModifier
{
    public function modify(string $output = ''): string;
}
