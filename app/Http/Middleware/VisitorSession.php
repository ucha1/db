<?php

namespace App\Http\Middleware;

use Closure;
use App\Events\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VisitorSession
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $id = $request->session()->getId();

        if (!$request->session()->has('visitor')) {
            $request->session()->push('visitor', $id);
            $request->session()->save();
        }

        $count = collect(Storage::disk('spath')->files())->filter(function($item){
            return $item !== ".gitignore";
        })->count();

        try {

            broadcast(new Visitor($count));
            //$request->session()->getHandler()->gc(5);

            return $next($request);
        } catch (\Exception $e) {
            return $next($request);
        }
    }
}
